<html>
<body>

<?php include("StanleyGibbonsPhase1-mathLib.php");

$arr = array(12,54,5,4,65,16,8,4); 

// Mean = The average of all the numbers 
echo 'Mean: '.SGmean($arr).'<br>'; 

// Median = The middle value after the numbers are sorted smallest to largest 
echo 'Median: '.SGmedian($arr).'<br>'; 

// Mode = The number that is in the array the most times 
echo 'Mode: '.SGmode($arr).'<br>'; 

// Range = The difference between the highest number and the lowest number 
echo 'Range: '.SGrange($arr); 

?>

</body>
</html>
