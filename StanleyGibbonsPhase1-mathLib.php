<?php

	/**
	* Takes input array and calculates the mathematical mean value.
	*
	* Mean shall be calculated by taking the sum of all the values of the array
	* divided by the total number of values in the array.
	* 
	* All returned values shall be rounded to max of 3 decimal places.
	* If a return value does not exist, return NULL
	* 
	* @param 1 dimensional array of numbers 
	* @return mean value of the numbers in the input array
	* @author Eric Faust
	*/
	function SGmean(array $a) {
		try {
			if(!is_array($a) || count($a) <= 0){ 
				return NULL; 
			}
			
			return round(array_sum($a) / count($a), 3);
		} catch (Exception $e) {
			return NULL;
		}
	}
	
	/**
	* Takes input array and calculates the mathematical median value.
	* 
	* Median shall be calculated by sorting the array into ascending order,
	* determining the middle index of the array by using half the number of
	* values in the array.
	*
	* All returned values shall be rounded to max of 3 decimal places.
	* If a return value does not exist, return NULL
	* 
	* @param 1 dimensional array of numbers 
	* @return median value of the numbers in the input array
	* @author Eric Faust
	*/
	function SGmedian(array $a) {
		try {
			if(!is_array($a) || count($a) <= 0){ 
				return NULL; 
			} 
			rsort($a); 
			$middle = round(count($a) / 2); 
			return round($a[$middle-1], 3);
		} catch (Exception $e) {
			return NULL;
		}
	}

	/**
	* Takes input array and calculates the mathematical mode value.
	* 
	* All returned values shall be rounded to max of 3 decimal places.
	* If a return value does not exist, return NULL
	* 
	* @param 1 dimensional array of numbers 
	* @return mode value of the numbers in the input array
	* @author Eric Faust
	*/
	function SGmode(array $a) {
		try {
			if(!is_array($a) || count($a) <= 0){ 
				return NULL; 
			}
			
			$count = array();
			// for each value in the valueArray
			foreach( $a as $value ) {
				if( isset( $count[$value] )) {
					$count[$value]++;
				} else {
					$count[$value] = 1;
				}
			}
			$mostCommon = "";
			$iter = 0;
			
			foreach( $count as $k => $v ) {
				if( $v > $iter ) {
					$mostCommon = $k;
					$iter = $v;
				}
			}
			return $mostCommon;
		} catch (Exception $e) {
			return NULL;
		}
	}
	
	/**
	* Takes input array and calculates the mathematical range value.
	* 
	* All returned values shall be rounded to max of 3 decimal places.
	* If a return value does not exist, return NULL
	* 
	* @param 1 dimensional array of numbers 
	* @return range value of the numbers in the input array
	* @author Eric Faust
	*/
	function SGrange(array $a) {
		try {
			if(!is_array($a) || count($a) <= 0){ 
				return NULL; 
			} 
			sort($a); 
            $sml = $a[0]; 
            rsort($a); 
            $lrg = $a[0]; 
            return round($lrg - $sml, 3); 
		} catch (Exception $e) {
			return NULL;
		}
	}
?>
