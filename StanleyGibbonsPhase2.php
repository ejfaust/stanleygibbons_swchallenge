<?php
	require "vendor/autoload.php"; 
 	include("StanleyGibbonsPhase1-mathLib.php");

	$app = new \Slim\Slim(); 
	
	/**
	 * When a POST request is called to "/mmmr" a single JSON
	 * object will be passed with an attribute called "numbers".
	 *
	 * The JSON file will be processed and we will return the 
	 * mean, median, mode and range of the incoming array in 
	 * a JSON object.
	 *
	 * Used Phalcon web framework
	 *
	 * 
	 */
	$app->post('/mmmr', function() use ($app) {

		$array = $app->request->getBody();

		$data = new Array();

		//Create a response
		$response = new \Slim\Slim();

		try{
			$mean = SGmean($array);
			$median = SGmedian($array);
			$mode = SGmode($array);
			$range = SGrange($array);
			
			$response->setBody(array("results" => ["mean" => $mean, "median" => $median, "mode" => $mode, "range" => $range])) ;
		}catch (Exception $e) {
			$response->setStatus(404);
			$response->setBody(array("error" => ["code" => 404, "message" => "Method GET not available on this endpoint"]));
		}
		return $response;
	});

	$app->run();
?>